#include <Adafruit_GPS.h>
#include <SoftwareSerial.h>

// Connect the GPS Power pin to 5V
// Connect the GPS Ground pin to ground
// Connect the GPS TX (transmit) pin to Digital 4
// Connect the GPS RX (receive) pin to Digital 2

SoftwareSerial mySerial(4, 2);
Adafruit_GPS GPS(&mySerial);

// Set GPSECHO to 'false' to turn off echoing the GPS data to the Serial console
// Set to 'true' if you want to debug and listen to the raw GPS sentences
#define GPSECHO  true

// this keeps track of whether we're using the interrupt
// off by default!
boolean usingInterrupt = false;
void useInterrupt(boolean); // Func prototype keeps Arduino 0023 happy


int ch1; // aileron
int ch2; // elevator
int ch3; // throttle
int ch4; // rudder
int sensorPin = A0;
int sensorValue = 0;
uint32_t timer = millis();

int rotor1=3, rotor2=10, rotor3=11, rotor4=9;
void setup() {
    pinMode(5, INPUT); // aileron
    pinMode(7, INPUT); // elevator
    pinMode(12, INPUT); // throttle
    pinMode(13, INPUT); // rudder
    
    Serial.begin(9600); 

    GPS.begin(9600);    

    GPS.sendCommand(PMTK_SET_NMEA_OUTPUT_RMCGGA);
//    GPS.sendCommand(PGCMD_ANTENNA);
    GPS.sendCommand(PMTK_SET_NMEA_UPDATE_1HZ); 

    // Request updates on antenna status, comment out to keep quiet
    GPS.sendCommand(PGCMD_ANTENNA);    
    
//    useInterrupt(true);
  
    delay(1000);  
    mySerial.println(PMTK_Q_RELEASE);    
}


// Interrupt is called once a millisecond, looks for any new GPS data, and stores it
SIGNAL(TIMER0_COMPA_vect) {
  char c = GPS.read();
  // if you want to debug, this is a good time to do it!
#ifdef UDR0
  if (GPSECHO)
    if (c) UDR0 = c;  
    // writing direct to UDR0 is much much faster than Serial.print 
    // but only one character can be written at a time. 
#endif
}

void useInterrupt(boolean v) {
  if (v) {
    // Timer0 is already used for millis() - we'll just interrupt somewhere
    // in the middle and call the "Compare A" function above
    OCR0A = 0xAF;
    TIMSK0 |= _BV(OCIE0A);
    usingInterrupt = true;
  } else {
    // do not call the interrupt function COMPA anymore
    TIMSK0 &= ~_BV(OCIE0A);
    usingInterrupt = false;
  }
}

void gps_read_result(void)                     // run over and over again
{
  // in case you are not using the interrupt above, you'll
  // need to 'hand query' the GPS, not suggested :(
  if (! usingInterrupt) {
    // read data from the GPS in the 'main loop'
    char c = GPS.read();
    // if you want to debug, this is a good time to do it!
    if (GPSECHO)
      if (c) Serial.print(c);
  }
  
  // if a sentence is received, we can check the checksum, parse it...
  if (GPS.newNMEAreceived()) {
    // a tricky thing here is if we print the NMEA sentence, or data
    // we end up not listening and catching other sentences! 
    // so be very wary if using OUTPUT_ALLDATA and trytng to print out data
  
    if (!GPS.parse(GPS.lastNMEA()))   // this also sets the newNMEAreceived() flag to false
      return;  // we can fail to parse a sentence in which case we should just wait for another
  }

  // if millis() or timer wraps around, we'll just reset it
  if (timer > millis())  timer = millis();

  // approximately every 2 seconds or so, print out the current stats
  if (millis() - timer > 2000) { 
    timer = millis(); // reset the timer
    
    Serial.print("\nTime: ");
    Serial.print(GPS.hour, DEC); Serial.print(':');
    Serial.print(GPS.minute, DEC); Serial.print(':');
    Serial.print(GPS.seconds, DEC); Serial.print('.');
    Serial.println(GPS.milliseconds);
    Serial.print("Date: ");
    Serial.print(GPS.day, DEC); Serial.print('/');
    Serial.print(GPS.month, DEC); Serial.print("/20");
    Serial.println(GPS.year, DEC);
    Serial.print("Fix: "); Serial.print((int)GPS.fix);
    Serial.print(" quality: "); Serial.println((int)GPS.fixquality); 
    if (GPS.fix) {
      Serial.print("Location: ");
      Serial.print(GPS.latitude, 4); Serial.print(GPS.lat);
      Serial.print(", "); 
      Serial.print(GPS.longitude, 4); Serial.println(GPS.lon);
      
      Serial.print("Speed (knots): "); Serial.println(GPS.speed);
      Serial.print("Angle: "); Serial.println(GPS.angle);
      Serial.print("Altitude: "); Serial.println(GPS.altitude);
      Serial.print("Satellites: "); Serial.println((int)GPS.satellites);
    }
  }
}


void loop() {
  ch1 = pulseIn(5, HIGH, 25000); // aileron
  ch2 = pulseIn(7, HIGH, 25000); // elevator
  ch3 = pulseIn(12, HIGH, 25000); // throttle
  ch4 = pulseIn(13, HIGH, 25000); // rudder
  
  sensorValue = analogRead(sensorPin);  
  
  ch3 = map(ch3, 976, 1800, 0, 250);  
  analogWrite(rotor1, ch3);
  analogWrite(rotor2, ch3);
  analogWrite(rotor3, ch3);
  analogWrite(rotor4, ch3);
  
  Serial.print("ch1(aileron): ");
  Serial.print(ch1); //
  Serial.print(" ch2(elevator): ");
  Serial.print(ch2); //
  Serial.print(" ch3(throttle): ");
  Serial.print(ch3); //
  Serial.print(" ch4(rudder): ");
  Serial.print(ch4); //
  Serial.print(" Light: ");
  Serial.print(sensorValue);   
  Serial.println();

  gps_read_result();
  Serial.println();    
  delay(3000);
}

